﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading;
using System.Net.Sockets;
using UnityEngine;
using System.Text;
using System;
using System.Linq;
using Newtonsoft.Json;
using TMPro;

public class ARPodio : MonoBehaviour {
	//TcpClient tcpClient;
    ClienteTcp clienteTcp;
    Thread tListener;
	public GameObject podioUnoGO;
	public GameObject podioDosGO;
	public GameObject podioTresGO;

    public Transform posChar1;
    public Transform posChar2;
    public Transform posChar3;

	public TextMeshPro notaUno;
	public TextMeshPro notaDos;
	public TextMeshPro notaTres;

	public TextMeshPro nombreUno;
	public TextMeshPro nombreDos;
	public TextMeshPro nombreTres;

	[SerializeField]
	private Transform oPosPodioUno;
	[SerializeField]
	private Transform oPosPodioDos;
	[SerializeField]
	private Transform oPosPodioTres;

    [SerializeField]
    private GameObject[] charactersHombre;

	[SerializeField]
	private GameObject[] charactersMujer;

    delegate void refrescarPodioCallBack(GameObject podioUnoGO, GameObject podioDosGO, GameObject podioTresGO, Estadistica estadistica);

	Estadistica pEstadistica = new Estadistica();

	//Esto es la relacion de reduccion de la escala de los podios, a mas pequeño mas grande los podios
	[SerializeField]
	float scaleRatio = 50f;

	//Este valor nos indica la velocidad total de "suavidad" que tendran los podios en actualizar su tamaño
	[SerializeField]
	float scaleDuration = 5;            

	// Use this for initialization
	void Start () {


        pEstadistica = new Estadistica();
        clienteTcp = new ClienteTcp(pEstadistica);
        tListener = new Thread(clienteTcp.Conectar);
        tListener.Name = "ThreadListener";
        tListener.Start();


    }

    void OnApplicationQuit() {
        clienteTcp.Desconectar();
    }

    void FixedUpdate() {
		if (pEstadistica.EsNuevo) 
			actualizarPodio (pEstadistica);
	}

	/// <summary>
	/// Actualizamos los tamaños de los podios de una forma suave y no que crezcan de golpe
	/// </summary>
	System.Object oLock = new System.Object();
	IEnumerator ScalePodios()
	{
		lock (oLock) {

			//Borramos los character si es que hay alguno
            if(posChar1.childCount > 0)
                Destroy(posChar1.GetChild(0).gameObject);
            if (posChar2.childCount > 0)
                Destroy(posChar2.GetChild(0).gameObject);
            if (posChar3.childCount > 0)
                Destroy(posChar3.GetChild(0).gameObject);
			
			List<Alumno> alumnos = new List<Alumno>(){
				new Alumno(){Nombre = pEstadistica.NombreUno, Promedio = pEstadistica.PromedioUno, Sexo = pEstadistica.SexoUno},
				new Alumno(){Nombre = pEstadistica.NombreDos, Promedio = pEstadistica.PromedioDos, Sexo = pEstadistica.SexoDos},
				new Alumno(){Nombre = pEstadistica.NombreTres, Promedio = pEstadistica.PromedioTres, Sexo = pEstadistica.SexoTres}};

			//Ordenamos por mejor promedio
			alumnos.Sort ((x, y) => -1* x.Promedio.CompareTo(y.Promedio));

			//Cargamos los nombres, character y notas
			notaUno.text = alumnos[0].Promedio.ToString();
			nombreUno.text = alumnos[0].Nombre;
			Instantiate (alumnos[0].Sexo ? charactersHombre[0] : charactersMujer[0],posChar1);
			//Instantiate (alumnos[0].Sexo ? charactersHombre[new System.Random().Next(0,charactersHombre.Length)] : charactersMujer[new System.Random().Next(0,charactersMujer.Length)],posChar1);

			notaDos.text = alumnos[1].Promedio.ToString();
			nombreDos.text = alumnos[1].Nombre;
			Instantiate (alumnos[1].Sexo ? charactersHombre[1] : charactersMujer[1],posChar2);

			notaTres.text = alumnos[2].Promedio.ToString();
			nombreTres.text = alumnos[2].Nombre;
			Instantiate (alumnos[2].Sexo ? charactersHombre[2] : charactersMujer[2],posChar3);

			//Reseteamos la pos original
			podioUnoGO.transform.localPosition = oPosPodioUno.localPosition;
			podioDosGO.transform.localPosition = oPosPodioDos.localPosition;
			podioTresGO.transform.localPosition = oPosPodioTres.localPosition;


			float valorYdeUnoCrecer = (float)alumnos [0].Promedio / scaleRatio;
			float valorYdeUnoMover = oPosPodioUno.localPosition.y + ((float)alumnos[0].Promedio / scaleRatio);

			float valorYdeDosCrecer = (float)alumnos [1].Promedio / scaleRatio;
			float valorYdeDosMover = oPosPodioDos.localPosition.y + ((float)alumnos[1].Promedio / scaleRatio);

			float valorYdeTresCrecer = (float)alumnos [2].Promedio / scaleRatio;
			float valorYdeTresMover = oPosPodioTres.localPosition.y + ((float)alumnos[2].Promedio / scaleRatio);

			//Actualizamos la escala de los podios de a poco para que sea un movimiento suave
            for (float t = 0; t < 1; t += Time.deltaTime / scaleDuration) {
				podioUnoGO.transform.localScale = Vector3.Lerp (podioUnoGO.transform.localScale,
					new Vector3 (podioUnoGO.transform.localScale.x, valorYdeUnoCrecer, podioUnoGO.transform.localScale.z) 
					, t);
				podioUnoGO.transform.localPosition = Vector3.Lerp (oPosPodioUno.localPosition,
					new Vector3(oPosPodioUno.localPosition.x, valorYdeUnoMover, oPosPodioUno.localPosition.z),
					t*2);

				podioDosGO.transform.localScale = Vector3.Lerp (podioDosGO.transform.localScale,
					new Vector3 (podioDosGO.transform.localScale.x, valorYdeDosCrecer, podioDosGO.transform.localScale.z),
					t);				
				podioDosGO.transform.localPosition = Vector3.Lerp (oPosPodioDos.localPosition,
					new Vector3(oPosPodioDos.localPosition.x, valorYdeDosMover, oPosPodioDos.localPosition.z),
					t*2);  
				

                podioTresGO.transform.localScale = Vector3.Lerp (podioTresGO.transform.localScale,
					new Vector3 (podioTresGO.transform.localScale.x, valorYdeTresCrecer, podioTresGO.transform.localScale.z),
					t);
				podioTresGO.transform.localPosition = Vector3.Lerp (oPosPodioTres.localPosition,
					new Vector3(oPosPodioTres.localPosition.x, valorYdeTresMover, oPosPodioTres.localPosition.z),
					t*2);

                yield return null;
			}
		}
	}

	//Hacemos la actualizacion de podios con una corutina
	void actualizarPodio(Estadistica estadistica){
		StartCoroutine(ScalePodios());

		//Le indicamos al FixedUpdate que el dato ahora es viejo
		pEstadistica.EsNuevo = false;
	}
}

public class Alumno{
	public String Nombre { get; set; }
	public Double Promedio { get; set; }
	public bool Sexo{ get; set; }
}

/// <summary>
/// Clase utilizada para transmitir datos entre servidor/cliente
/// </summary>
public class Estadistica
{
	public String NombreUno { get; set; }
	public Double PromedioUno { get; set; }
	public bool SexoUno{ get; set; }

    public String NombreDos { get; set; }
	public Double PromedioDos { get; set; }
	public bool SexoDos { get; set; }

    public String NombreTres { get; set; }
	public Double PromedioTres { get; set; }
	public bool SexoTres { get; set; }

    public bool EsNuevo { get; set; }

	public String Serializar()
	{
		return JsonConvert.SerializeObject(this);
	}
}

public class ClienteTcp
{
    //thread para el reader
    Thread reader;

    TcpClient tcpCliente;
    
    NetworkStream stream;
    StreamReader sReader;
    Estadistica pEstadistica;

    public ClienteTcp(Estadistica estadistica) //, CancellationToken _ct
    {
        pEstadistica = estadistica;
    }

    public void Desconectar() {
        if(sReader != null) sReader.Close();
        if(stream!=null) stream.Close();
        tcpCliente.Close();
    }

    public void Conectar()
    {
        try
        {
            int timeOut = 3; //TimeOut del connect
            //tcpCliente = new TcpClient("127.0.0.1", 8888);
            tcpCliente = new TcpClient();
            IAsyncResult ar = tcpCliente.BeginConnect("127.0.0.1", 8888, null, null);
            System.Threading.WaitHandle wh = ar.AsyncWaitHandle;
            try
            {
                if (!ar.AsyncWaitHandle.WaitOne(TimeSpan.FromSeconds(timeOut), false))
                {
                    tcpCliente.Close();
                    Debug.Log("TimeOut al conectar al servidor ");
                }

                tcpCliente.EndConnect(ar);
            }
            catch (Exception ex)
            {
                Debug.Log("Error no esperado esperando la conexion: " + ex.Message);
            }

            //ct = _ct;
            stream = tcpCliente.GetStream();
            sReader = new StreamReader(stream, Encoding.ASCII);

            Debug.Log("Conectado al sevidor");

            //si todo va bien inicio el hilo para escuchar
            reader = new Thread(this.Escuchar);
            reader.Name = "TCP Reader";
            reader.Start();
        }
        catch (Exception ex)
        {
            Debug.Log("Error no esperado al conectar al servidor: " + ex.Message);
        }
    }

    private void Escuchar()
    {
        try
        {
            while (true)
            {
                try
                {
                    if (sReader == null)
                    {
                        Debug.Log("sReader null, fallo al conectar");
                        break;
                    }
                    // espero que me lleguen datos
                    string datos = sReader.ReadLine();
                    if (datos == null)
                    {
                        Debug.Log("ReadLine retorno null, desconexión detectada");
                        break;
                    }
                    //Convierto los datos de JSON a objeto
                    Estadistica newEstadistica = JsonConvert.DeserializeObject<Estadistica>(datos);
                    if (newEstadistica == null)
                    {
                        Debug.Log("DeserializeObject retorno null, fallo el metodo");
                    }
                    else
                    {
                        ////Mapeamos los datos recibidos, lo ideal seria tener un mapper pero bueno xD
                        pEstadistica.PromedioUno = newEstadistica.PromedioUno;
                        pEstadistica.NombreUno = newEstadistica.NombreUno;
                        pEstadistica.PromedioDos = newEstadistica.PromedioDos;
                        pEstadistica.NombreDos = newEstadistica.NombreDos;
                        pEstadistica.PromedioTres = newEstadistica.PromedioTres;
                        pEstadistica.NombreTres = newEstadistica.NombreTres;
                        pEstadistica.EsNuevo = newEstadistica.EsNuevo;
                    }
                }
                catch (Exception ex)
                {
                    Debug.Log("Error no esperado en ReadLine o JsonConvert : " + ex.StackTrace);
                    break;
                }
            }
        }
        catch (Exception ex)
        {
            Debug.Log("Error no esperado en escuchar : " + ex);
        }
        finally
        {
            sReader.Close();
            stream.Close();
            tcpCliente.Close();
        }
    }
}
